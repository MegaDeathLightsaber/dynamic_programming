import java.awt.Color
import java.awt.image.BufferedImage
import java.io.File
import javax.imageio.ImageIO

/**
 * Represents two-dimensional matrix.
 */
typealias Matrix2D<T> = Array<Array<T>>

/**
 * Represents a generic pair of two homogeneous values.
 */
typealias Vec2D<T> = Pair<T, T>

/**
 * Domain, where dynamic programming runs.
 * Rectangular field, each cell of which contains a natural number.
 */
class Field(private val field: Matrix2D<Int>) {
    /**
     * Solution of subtask.
     * @param weight Sum of cell values in route.
     * @param task Corresponding subtask.
     */
    data class Result(val weight: Int, val task: Vec2D<Int>, val next: Result?) : Comparable<Result> {
        override fun compareTo(other: Result): Int {
            return weight.compareTo(other.weight)
        }

        companion object {
            val neutral = Result(Int.MAX_VALUE, Pair(-1, -1), null)
        }
    }

    /**
     * Contains memorized subtask data.
     */
    private val cache = HashMap<Vec2D<Int>, Result>()

    init {
        if (field.isNotEmpty()) {
            val endingSubTask = Vec2D(field.last().lastIndex, field.lastIndex)
            cache[endingSubTask] = Result(field.last().last(), endingSubTask, null)
        }
    }

    /**
     * Solves task for given starting point.
     * @return Solution for required subtask.
     */
    fun findMinRoute(x: Int = 0, y: Int = 0): Result {
        val subTask = Vec2D(x, y)

        if (y !in field.indices || x !in field.last().indices) {
            return Result.neutral
        }

        if (subTask !in cache) {
            val lookup = minOf(
                findMinRoute(x + 1, y),
                findMinRoute(x, y + 1),
                findMinRoute(x + 1, y + 1)
            )

            val result = Result(
                weight = field[y][x] + lookup.weight,
                task = subTask,
                next = lookup
            )

            cache[subTask] = result
        }

        return cache.getOrDefault(subTask, Result.neutral)
    }

    /**
     * Displays solution to image file with given name.
     * @throws IllegalStateException if field area larger than [FIELD_SIZE_CONSTRAINT] (commonly, 1000*1000).
     */
    fun render(fileName: String, result: Result) {
        val resultPath = result.getPath()

        if ((field.last().lastIndex + 1) * (field.lastIndex + 1) > FIELD_SIZE_CONSTRAINT) {
            throw IllegalStateException("Can't render big images due to JVM limitations")
        }

        val actualSize = (resultPath.size * resultPath.size * SCALE_KOEF + TILE_SIZE).toInt()

        val width = (field.last().lastIndex + 1) * actualSize
        val height = (field.lastIndex + 1) * actualSize
        val image = BufferedImage(width, height, BufferedImage.TYPE_INT_RGB)

        val path = resultPath.map {
            Pair(
                it.first * actualSize + actualSize / 2,
                it.second * actualSize + actualSize / 2
            )
        }

        image.graphics.run {
            color = Color.WHITE
            fillRect(0, 0, width, height)

            color = Color.BLACK
            for (x in field.last().indices) {
                drawLine(x * actualSize, 0, x * actualSize, height)
            }

            for (y in field.indices) {
                drawLine(0, y * actualSize, width, y * actualSize)
            }

            for (x in field.last().indices) {
                for (y in field.indices) {
                    drawString(
                        "${field[y][x]}",
                        x * actualSize + actualSize / 2,
                        y * actualSize + actualSize / 2
                    )
                }
            }

            val (xPoints, yPoints) = path.unzip()
            val nPoints = path.size

            color = Color.RED
            drawPolyline(xPoints.toIntArray(), yPoints.toIntArray(), nPoints)
            path.forEach {
                fillOval(
                    it.first - MARKER_RADIUS,
                    it.second - MARKER_RADIUS,
                    MARKER_RADIUS * 2,
                    MARKER_RADIUS * 2
                )
            }
            drawString("${result.weight}", TILE_SIZE / 4, TILE_SIZE / 4)

            dispose()
        }

        val file = File(fileName)
        val (_, format) = fileName.split('.')
        ImageIO.write(image, format, file)
    }

    companion object {
        private const val TILE_SIZE = 64
        private const val SCALE_KOEF = -0.00000075 * TILE_SIZE
        private const val MARKER_RADIUS = 2
        private const val FIELD_SIZE_CONSTRAINT = 1000 * 1000
    }
}

fun Field.Result?.getPath(): List<Vec2D<Int>> {
    val list = mutableListOf<Vec2D<Int>>()

    var current = this
    while (current != null) {
        list.add(current.task)
        current = current.next
    }

    return list
}