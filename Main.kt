import java.io.File
import java.io.PrintWriter
import java.util.*

fun main() {
    val outputWriter = PrintWriter(File("output.dat"))
    val scanner = Scanner(File("input.dat"))
    with(scanner) {
        var testNo = 1
        while (scanner.hasNext()) {
            val n = scanner.nextInt()
            val m = scanner.nextInt()

            val cells = Array(n) { Array(m) { 0 } }

            for (i in 0 until n) {
                for (j in 0 until m) {
                    val cell = scanner.nextInt()
                    cells[i][j] = cell
                }
            }

            val field = Field(cells)
            val result = field.findMinRoute()
            val path = result.getPath()

            println("Test #$testNo:")
            println("Minimal weight: ${result.weight}")
            println("Route: $path")

            outputWriter.println("Test #$testNo:")
            outputWriter.println("Minimal weight: ${result.weight}")
            outputWriter.println("Route: $path")

//            Uncomment for result rendering
//            if (path[0] != Vec2D(-1, -1)) {
//                field.render("test_$testNo.png", result)
//            }

            testNo++
        }
        close()
    }
    outputWriter.close()
}
